// Cerebellum
// =======================================================
// Christoph Luehr <luehr@r-pentomino.de>

#include <SerialCommand.h>
#include <Button.h>
#include <Rotary.h>
#include <TempHumidity.h>


#define PIN_LED 13   // Arduino LED on board
#define PIN_BUTTON 2
#define PIN_ROTARY A0 // analog!

#define PIN_TEMPHUMIDITY 8


SerialCommand serialCommand;     // The demo SerialCommand object
Button button;
Rotary rotary;

TempHumidity tempHumidity(8, DHT11);

void setup() {
  pinMode(PIN_LED, OUTPUT);      // Configure the onboard LED for output
  digitalWrite(PIN_LED, LOW);    // default to LED off

  button.setup(PIN_BUTTON);
  rotary.setup(PIN_ROTARY);
  tempHumidity.setup();

  Serial.begin(9600);

  // Setup callbacks for SerialCommand commands
  serialCommand.addCommand("LED",    onCmdLed);          // Turns LED on
  serialCommand.addCommand("HELLO", sayHello);        // Echos the string argument back
  serialCommand.addCommand("P",     processCommand);  // Converts two arguments to integers and echos them back
  serialCommand.addCommand("BUTTON",    onCmdButton);
  serialCommand.addCommand("ROTARY",    onCmdRotary);
  serialCommand.addCommand("TEMP",    onCmdTemp);
  serialCommand.addCommand("NOP",    onCmdNop);

  serialCommand.setDefaultHandler(unrecognized);      // Handler for command that isn't matched  (says "What?")
  Serial.println("READY");
}

void loop() {
  
  serialCommand.readSerial();     // We don't do much, just process serial commands
   
  if (button.isClicked()) {
	Serial.println("BUTTON CLICK 1");
  }
}

void onCmdButton() {
    if (button.isPressed()) {
	Serial.println("BUTTON PRESS 1");
    } else {
      Serial.println("BUTTON PRESS 0");
    }
}

void onCmdNop() {
    Serial.println("NOP");
}

void onCmdTemp() {
    float h = tempHumidity.readHumidity();
    float t = tempHumidity.readTemperature();

  // check if returns are valid, if they are NaN (not a number) then something went wrong!
  if (isnan(t) || isnan(h)) {
    Serial.println("Failed to read from Temperature&Humidity Sensor");
  } else {
    Serial.print("Humidity: "); 
    Serial.print(h);
    Serial.print(" %\t");
    Serial.print("Temperature: "); 
    Serial.print(t);
    Serial.println(" *C");
  }


}

void onCmdRotary() {

  Serial.print("ROTARY ");
  Serial.println(rotary.getDegree());
  
  // Serial.print("MAPPED (int): ");
  // Serial.println(myRotary.getMappedValue(10, 20));
}


void onCmdLed() {
  
  int val;
  char *arg;
  
  arg = serialCommand.next();    // Get the next argument from the SerialCommand object buffer
  
  if (arg == NULL) {
    Serial.println("LED ERROR");
    return;
  }
  
  val = atoi(arg);    // Converts a char string to an integer
  
  if (val == 1) {
      digitalWrite(PIN_LED, HIGH);
      Serial.println("LED 1");
  }
  if (val == 0) {
      digitalWrite(PIN_LED, LOW);
      Serial.println("LED 0");
  }
}


void sayHello() {
  char *arg;
  arg = serialCommand.next();    // Get the next argument from the SerialCommand object buffer
  if (arg != NULL) {    // As long as it existed, take it
    Serial.print("Hello ");
    Serial.println(arg);
  }
  else {
    Serial.println("Hello, whoever you are");
  }
}


void processCommand() {
  int aNumber;
  char *arg;

  Serial.println("We're in processCommand");
  arg = serialCommand.next();
  if (arg != NULL) {
    aNumber = atoi(arg);    // Converts a char string to an integer
    Serial.print("First argument was: ");
    Serial.println(aNumber);
  }
  else {
    Serial.println("No arguments");
  }

  arg = serialCommand.next();
  if (arg != NULL) {
    aNumber = atol(arg);
    Serial.print("Second argument was: ");
    Serial.println(aNumber);
  }
  else {
    Serial.println("No second argument");
  }
}

// This gets set as the default handler, and gets called when no other command matches.
void unrecognized(const char *command) {
  Serial.println("What?");
}
